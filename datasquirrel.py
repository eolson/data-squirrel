#!/usr/bin/python

# Data Squirrel
# Written by Eric Olson 2017
# Take mysql database dumps and organizes backups

import yaml, os, MySQLdb, time
from datetime import datetime, timedelta
from subprocess import Popen, PIPE

# Read Configuration
SCRIPT_PATH = os.path.dirname(os.path.abspath( __file__ ))
try:
    with open(SCRIPT_PATH + "/local.yml", 'r') as ymlfile:
        SQUIRREL_CFG = yaml.load(ymlfile)
except:
    print "Error: No local config present (local.yml), Copy config.yml to local.yml and edit.\n"
    quit()

# Start date for calculating backup management.
# Date should NOT be changed once backups have started.
REFERENCE_DATE = datetime.strptime('20170102', '%Y%m%d')

def create_backup_folder(dbhost):
    global SQUIRREL_CFG
    dbdirectory = SQUIRREL_CFG['backupdir'] + '/' + dbhost
    try:
        if not os.path.exists(dbdirectory):
            os.makedirs(dbdirectory)
    except:
        print("Could not create backup directory: %s" % dbdirectory)
        quit()

def list_those_dbs(dbhost):
    global SQUIRREL_CFG
    dblist = []
    dbserv = MySQLdb.connect(host=dbhost, user=SQUIRREL_CFG['mysql']['user'], passwd=SQUIRREL_CFG['mysql']['passwd'])
    c = dbserv.cursor()
    c.execute("SHOW DATABASES")
    result = c.fetchall()
    c.close()
    for singledb in result:
        dblist.append(singledb[0])
    return dblist

def now_clean_those_dumps(dbhost, dbdirectory, singledb, stamptime):
    global SQUIRREL_CFG
    global REFERENCE_DATE
    stamptimeobj = datetime.strptime(stamptime, '%Y%m%d')
    for dbfile in os.listdir(dbdirectory):
        userdelta1 = SQUIRREL_CFG['dailyback']
        userdelta2 = SQUIRREL_CFG['everyotherback']
        userdelta3 = SQUIRREL_CFG['everyfourthback']
        dbfiledate = dbfile.split('-')[-1].strip('.gz')
        try:
            dateobj = datetime.strptime(dbfiledate, '%Y%m%d')
        except:
            print("Skipping file: %s" % dbdirectory + '/' + dbfile)
            continue
        
        deletefile = dbdirectory + '/' + dbfile
        delta = stamptimeobj - dateobj
        delta_reference = REFERENCE_DATE - dateobj

        if not delta_reference.days % 2 == 0 and delta.days >= userdelta1 and delta.days <= userdelta2:
            print("Deleting: %s" % deletefile)
            os.remove(deletefile)
        elif not delta_reference.days % 4 == 0 and delta.days >= userdelta2 and delta.days <= userdelta3:
            print("Deleting: %s" % deletefile)
            os.remove(deletefile)
        elif delta.days > userdelta3:
            print("Deleting: %s" % deletefile)
            os.remove(deletefile)
        print("Keeping: %s" % deletefile)

def dump_those_dbs(dbhost, dblist, stamptime):
    global SQUIRREL_CFG
    dbignorelist = SQUIRREL_CFG['dbignorelist'].split(',')
    dbignorelist = [x.strip() for x in dbignorelist]
    for singledb in dblist:
        if singledb not in dbignorelist:
            dbdirectory = SQUIRREL_CFG['backupdir'] + '/' + dbhost + '/' + singledb
            try:
                if not os.path.exists(dbdirectory):
                    os.makedirs(dbdirectory)
            except:
                print("Could not create backup directory: %s" % dbdirectory)
                continue
            dbfilename = SQUIRREL_CFG['backupdir'] + '/' + dbhost + '/' + singledb + '/' + singledb + '-' + stamptime + '.gz'
            dumpargs = ['mysqldump', '-h', dbhost, '-u', SQUIRREL_CFG['mysql']['user'], '-p' + SQUIRREL_CFG['mysql']['passwd'], '--databases', singledb]
            with open(dbfilename, 'wb', 0) as f:
                p1 = Popen(dumpargs, stdout=PIPE)
                p2 = Popen('gzip', stdin=p1.stdout, stdout=f)
            p1.stdout.close() # force write error (/SIGPIPE) if p2 dies
            p2.wait()
            p1.wait()
            now_clean_those_dumps(dbhost, dbdirectory, singledb, stamptime)

def main():
    global SQUIRREL_CFG
    stamptime = time.strftime('%Y%m%d')
    hostlist = SQUIRREL_CFG['hostlist'].split(',')
    hostlist = [x.strip() for x in hostlist]
    for dbhost in hostlist:
        create_backup_folder(dbhost)
        dblist = list_those_dbs(dbhost)
        dump_those_dbs(dbhost, dblist, stamptime)

main()
